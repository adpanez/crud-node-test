# README #

CRUD de usuarios que consta de las siguientes funcionalidades: Listar, agregar, actualizar y eliminar.

### Levantar el proyecto ###

* Instala las dependencias
`yarn install`

* Corre la aplicación
Para el uso local
`yarn start:dev`

* Para un entorno de producción
`yarn start`

En el package.json se definen variables de entorno para el entorno de producción

### Recursos ###
* Se una base de datos no relacional: MongoDB. Ir a la documentación sobre [mongoDB](https://docs.mongodb.com/manual/administration/install-community/) Para información sobre como instalar y ejecutar MONGO

* Se utiliza `mongoose` para modelar los datos antes de persistirlos en la mongoDB

* Se usa `express-fileupload` para el tratamiento de las imagenes

* Se usa `joi` para validar la entrada de datos en el body