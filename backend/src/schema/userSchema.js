'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  name: String,
  lastname: String,
  email: String,
  birthday: String,
  gender: String,
  phone: Number,
  logo: String,
  active: Boolean,
});

module.exports = mongoose.model('User', userSchema);