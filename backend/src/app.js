'use strict';
const express = require('express');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const routerSwagger= require('./swagger/swagger');
const cors = require('cors');
const path = require('path');
const { DB_URL, PORT, STATIC_FOLDER } = require('./config');
const routes = require('./routes/routes');

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true
}).then(() => {
  const app = express();

  app.use(express.json());

  app.use(fileUpload());

  app.use(cors());

  app.use(routes);

  app.use(routerSwagger);

  app.use('/public', express.static(path.join(__dirname, STATIC_FOLDER)));

  app.listen(PORT, () => {
    console.log('server running in http://localhost:' + PORT)
  })
});

mongoose.connection.on('error', err => {
  console.error(err);
  console.log('MongoDB connection failed: ' + db);
  process.exit();
});