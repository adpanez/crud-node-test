'use strict';
const userService = require('../services/userServices');
const userValidate = require('../validates/userValidate');

exports.allUsers = async (_, res) => {
  try {
    const usersReponse = await userService.getAllUsers();
    return res.send(usersReponse);
  } catch (err) {
    res.status(500).send(err);
  }
}

exports.createUser = async (req, res) => {
  const { body } = req;

  const { error } = userValidate.validate(body);

  if (error) {
    res.status(500).send(error.details);
  }

  try {
    const createdUserResponse = await userService.createUser(body);
    return res.send(createdUserResponse);
  } catch (err) {
    res.status(500).send(err)
  }
}

exports.getUserById = async (req, res) => {
  const { id } = req.params;

  try {
    const userResponse = await userService.getUserById(id);
    return res.send(userResponse);
  } catch (err) {
    res.status(500).send(err);
  }
}

exports.uploadLogo = async (req, res) => {
  const { id } = req.params;
  const files = req.files || null;

  try {
    const uploadedLogoResponse = await userService.uploadLogo(id, files);

    res.send(uploadedLogoResponse);
  } catch (err) {
    res.status(500).send(err);
  }
}

exports.updateUser = async (req, res, next) => {
  const { id } = req.params;
  const body = req.body;

  try {
    const updatedUserResponse = await userService.updateUser(id, body);

    res.send(updatedUserResponse)
  } catch (err) {
    res.status(500).send(err);
  }
}

exports.deleteUser = async (req, res) => {
  const { id } = req.params;

  try {
    const deletedUserResponse = await userService.deleteUser(id);
    return res.send(deletedUserResponse);
  } catch (err) {
    res.status(500).send(err)
  }
}

exports.getPrimeNumbers = (req, res) => {
  // const number = req.body.number;
  const { id } = req.params;

  const primeNumbersResponse = userService.getPrimeNumbers(id);

  res.send(primeNumbersResponse);
}