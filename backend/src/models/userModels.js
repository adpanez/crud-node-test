'use strict';
const User = require('../schema/userSchema');

exports.getAllUsers = () => {
  const filter = {};

  return User.find(filter, (err, doc) => {
    if (err) {
      return err
    }

    return doc;
  })
}

exports.getUserById = (id) => {
  return User.findById(id, (err, doc) => {
    if (err) {
      return err;
    }

    return doc;
  })
}

exports.getUserByEmail = (email) => {
  return User.findOne({email}, (err, doc) => {
    if (err) {
      return err
    }

    return doc;
  })
}

exports.createUser = (user) => {
  const newUser = new User(user);

  return newUser.save((err, doc) => {
    if (err) {
      return err
    }
    return doc;
  });
}

exports.deleteUser = (id) => {
  return User.findByIdAndDelete(id, (err, doc) => {
    if (err) {
      return err
    }

    return doc;
  });
}

exports.updateUser = (id, data) => {
  return User.findByIdAndUpdate({_id: id}, data, (err, doc) => {
    if (err){
      return err
    }
    return doc;
  })
}