'use strict';
const Joi = require('joi');
const userValidateschema = Joi.object({
  name: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  lastname: Joi.string()
    .alphanum()
    .min(3)
    .max(60)
    .required(),
  email: Joi.string()
    .email()
    .required(),
  birthday: Joi.date()
    .raw(),
  gender: Joi.string()
    .valid('male', 'female')
    .required(),
  phone: Joi.number(),
  active: Joi.boolean()
    .required()
});

module.exports = userValidateschema;