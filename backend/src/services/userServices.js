'use strict';
const userModel = require('../models/userModels');
const { validateFiles, moveFile } = require('../utils/handleFiles');
const { getPrimeNumbers } = require('../utils/utils');

exports.getAllUsers = async () => {
  try {
    const users = await userModel.getAllUsers();
    return { data: users, status: 'OK' };
  } catch (err) {
    return err
  }
}

exports.getUserById = async (id) => {
  try {
    const user = await userModel.getUserById(id);
    return { data: user, status: 'OK' }
  } catch (err) {
    return err;
  }
}

exports.uploadLogo = async (id, files) => {
  const PREFIX_IMAGE = '.jpg';
  const allowedPrefixes = ['image/jpg', 'image/jpeg'];

  try {

    const validatingFile = validateFiles(files, allowedPrefixes);

    if (validatingFile.status === 'FORBIDDEN') {
      return validatingFile;
    } else {
      const uploadPath = `./public/${id}${PREFIX_IMAGE}`;
      const dataToUpdate = {logo: uploadPath};

      const statusFileMV = await moveFile(files.logo, uploadPath);

      if (statusFileMV.status === 'OK') {
        const updatedUser = await userModel.updateUser(id, dataToUpdate);

        return { data: updatedUser, status: 'OK' }
      } else {
        return statusFileMV;
      }
    }
  } catch (err) {
    return err;
  }
}

exports.createUser = async (user) => {
  let existingUser = null;

  try {
    existingUser = await userModel.getUserByEmail(user.email);

    if (existingUser) {
      return { message: 'El email ya esta en uso', status: 'FORBIDDEN' };
    }
  } catch (err) {
    return err;
  }

  try {
    const newUser = await userModel.createUser({...user, logo: ''});

    return { data: newUser, status: 'OK' };
  } catch (err) {
    return err;
  }
}

exports.updateUser = async (id, user) => {
  try {
    const updatedUser = await userModel.updateUser(id, user);

    return { data: updatedUser, status: 'OK' };
  } catch (err) {
    return err;
  }
}

exports.deleteUser = async (id) => {
  try {
    const deletedUser = await userModel.deleteUser(id);

    return { data: deletedUser, status: 'OK' };
  } catch (err) {
    return err;
  }
}

exports.getPrimeNumbers = (number) => {
  const primeNumbers = getPrimeNumbers(number);

  return { data: primeNumbers, status: 'OK' };
}