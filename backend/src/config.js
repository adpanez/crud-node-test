const STATIC_FOLDER = 'public';

// MONGODB config
const MONGO_HOSTNAME = process.env.MONGO_HOSTNAME || '127.0.0.1';
const MONGO_PORT = process.env.MONGO_PORT || 27017;
const MONGO_DB = process.env.MONGO_DB || 'crud_users';

const DB_URL = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;

// APP config
const PORT = process.env.PORT || 8080;

module.exports = { STATIC_FOLDER, DB_URL, PORT };