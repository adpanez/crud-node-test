'use strict';

exports.validateFiles = (files, prefix) => {
  if (!files) {
    return { message: 'No hay archivos para subir', status: 'FORBIDDEN' };
  } else if (!prefix.includes(files.logo.mimetype)) {
    return { message: 'El formato del archivo no es el correcto', status: 'FORBIDDEN' }
  } else {
    return { status: 'OK' }
  }
}

exports.moveFile = (file, uploadPath) => {
  file.mv(uploadPath, err => {
    if (err) {
      return { err: 'Hubo un error con el archivo', status: 'INTERNAL_ERROR' }
    }
  })
  return { status: 'OK' }
}