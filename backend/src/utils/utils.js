'use strict';
exports.getPrimeNumbers = (number) => {
  const primeNumbers = [];

  for(let i = 2; i < number; i++) {
    if (number % i !== 0) {
      primeNumbers.push(i);
    }
  }

  return primeNumbers;
}