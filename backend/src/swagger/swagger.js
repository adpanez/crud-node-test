const express = require('express');
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const { PORT } = require('../config');

const routerSwagger = express.Router();

const options = {
  swaggerDefinition: {
    swagger: '2.0',
    info: {
      title: 'CRUD API',
      version: '1.0.0',
      description: 'A simple express crud api'
    },
    tags: [
      {
        name: 'User Crud',
        description: 'Users API'
      }
    ],
    servers: [
      {
        url: 'http://localhost:' + PORT
      }
    ]
  },
  apis: ['../routes/routes.js']
}

const specs = swaggerJSDoc(options);

routerSwagger.use('/api-docs', swaggerUI.serve);
routerSwagger.get('/api-docs', swaggerUI.setup(specs, { explorer: true }));

module.exports = routerSwagger;