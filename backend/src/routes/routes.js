'use strict';
const express = require('express');
const userController = require('../controllers/userControllers');

const router = express.Router();

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Retrieve a list of JSONPlaceholder users
 *     description: Retrieve a list of users from JSONPlaceholder. Can be used to populate a list of fake users when prototyping or testing an API.
*/
router.get('/user', userController.allUsers);

router.get('/user/:id', userController.getUserById);

router.post('/upload-logo/:id', userController.uploadLogo);

router.post('/user', userController.createUser);

router.put('/user/:id', userController.updateUser);

router.delete('/user/:id', userController.deleteUser);

router.get('/user/prime-numbers/:id', userController.getPrimeNumbers);

module.exports = router;