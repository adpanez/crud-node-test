import { IUser, IUserResponseSuccess, Status } from '../interfaces/userInterface';
import { getUserByEmail, createUser, getAllUsers } from '../model/userModel';

export const createUserService = async (user: IUser): Promise<IUserResponseSuccess> => {
  let existingUser: IUser | null = null;

  try {
    existingUser = await getUserByEmail(user.email);

    if (existingUser) {
      return { message: 'El email ya esta en uso', status: Status.FORBIDDEN };
    }
  } catch (err: any) {
    return err;
  }

  try {
    const newUser = await createUser({...user, logo: ''});

    return { data: newUser, status: Status.OK };
  } catch (err: any) {
    return err;
  }
}

export const getAllUsersService = async (): Promise<IUserResponseSuccess> => {
  try {
    const users = await getAllUsers();
    return { data: users, status: Status.OK };
  } catch (err: any) {
    return err
  }
}