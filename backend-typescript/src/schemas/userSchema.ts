import { Schema, model } from 'mongoose';
import { IUser } from '../interfaces/userInterface';

const userSchema = new Schema<IUser>({
  name: String,
  lastname: String,
  email: String,
  birthday: String,
  gender: String,
  phone: Number,
  logo: String,
  active: Boolean,
});

export const User = model<IUser>('User', userSchema);