import { Request, Response } from 'express';
import { IUserResponseSuccess } from '../interfaces/userInterface';
import { createUserService, getAllUsersService } from '../services/userServices';

export const createUserController = async (req: Request, res: Response): Promise<void> => {
  const { body } = req;

  try {
    const createdUserResponse: IUserResponseSuccess = await createUserService(body);
    res.send(createdUserResponse);
  } catch (err: any) {
    res.status(500).send(err)
  }
}

export const allUsersController = async (_: Request, res: Response) => {
  try {
    const usersReponse: IUserResponseSuccess = await getAllUsersService();
    return res.send(usersReponse);
  } catch (err) {
    res.status(500).send(err);
  }
}