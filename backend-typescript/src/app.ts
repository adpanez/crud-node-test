import express from 'express';
import mongoose from 'mongoose';
import fileUpload from 'express-fileupload';
import cors from 'cors';
import path from 'path';
import { DB_URL, PORT, STATIC_FOLDER } from './config';
import { routes } from './routes/routes';

mongoose.connect(DB_URL, (err) => {
  if (err) {
    console.log('MongoDB connection failed: ' + err);
    process.exit();
  }
  else {
    const app = express();

    app.use(express.json());
  
    app.use(fileUpload());
  
    app.use(cors());
  
    app.use(routes);
  
    app.use('/public', express.static(path.join(__dirname, STATIC_FOLDER)));
  
    app.listen(PORT, () => {
      console.log('server running in http://localhost:' + PORT)
    })
  }
});