import { CallbackError } from "mongoose";

export interface IUser {
  name: string,
  lastname: string,
  email: string,
  birthday: string,
  gender: string,
  phone: string,
  logo?: string,
  active: string,
}

export interface IUserFindByKey {
  [key: string]: string
}

export type IUserResultQuery = CallbackError | IUser | IUser[];

export enum Status {
  FORBIDDEN, OK
}

interface IUserStatusResponse {
  status: Status
}

// interface IUserDataResponse<T>
interface IUserDataResponse extends IUserStatusResponse {
  data: any
}

interface IUserMessageResponse extends IUserStatusResponse {
  message: string
}

export type IUserResponseSuccess = IUserDataResponse | IUserMessageResponse;