import { Router } from 'express';
import { allUsersController, createUserController } from '../controllers/userControllers';

export const routes = Router();

routes.post('/user', createUserController);

routes.get('/users', allUsersController);