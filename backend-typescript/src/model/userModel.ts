import { CallbackError } from 'mongoose';
import { IUser, IUserFindByKey, IUserResultQuery } from '../interfaces/userInterface';
import { User } from '../schemas/userSchema';

export const getAllUsers = () => {
  const filter = {};

  return User.find(filter, (err: CallbackError, docs: IUser[] | null): IUserResultQuery => {
    if (err) {
      return err
    }

    return docs;
  })
}


export const getUserByEmail = (email: string) => {
  const queryEmail: IUserFindByKey = {email}
  return User.findOne(queryEmail, (err: CallbackError, doc: IUser | null): IUserResultQuery => {
    if (err) {
      return err
    }

    return doc;
  })
}

export const createUser = (user: IUser) => {
  const newUser = new User(user);

  newUser.save((err: CallbackError, doc: IUser | null): IUserResultQuery => {
    if (err) {
      return err
    }

    return doc;
  });
}