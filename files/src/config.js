// MONGODB config
const MONGO_HOSTNAME = process.env.MONGO_HOSTNAME || '127.0.0.1';
const MONGO_PORT = process.env.MONGO_PORT || 27017;
const MONGO_DB = process.env.MONGO_DB || 'crud_users';

const DB_URL = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
// const DB_URL = `mongodb+srv://crud-user:123@cluster0.57k5m.mongodb.net/crud_images?retryWrites=true&w=majority`

// APP config
const PORT = process.env.PORT || 5000;

module.exports = { DB_URL, PORT };