const express = require('express');
const router = express.Router();
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const crypto = require('crypto');
const path = require('path');
const mongoose = require('mongoose');

const Image = require('../schemas/imageSchema');
const { DB_URL } = require('../config');

const connect = mongoose.createConnection(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

let gfs;

connect.once('open', () => {
    // initialize stream
    gfs = new mongoose.mongo.GridFSBucket(connect.db, {
        bucketName: "uploads"
    });
});

const storage = new GridFsStorage({
  url: DB_URL,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
          if (err) {
              return reject(err);
          }
          const filename = buf.toString('hex') + path.extname(file.originalname);
          const fileInfo = {
              filename: filename,
              bucketName: 'uploads'
          };
          resolve(fileInfo);
      });
    });
  }
});

const upload = multer({ storage });


router.post('/upload-file', upload.single('file'), (req, res, next) => {
  console.log(req.body);
  // check for existing images
  Image.findOne({ caption: req.body.caption })
      .then((image) => {
          console.log(image);
          if (image) {
              return res.status(200).json({
                  success: false,
                  message: 'Image already exists',
              });
          }

          let newImage = new Image({
              caption: req.body.caption,
              filename: req.file.filename,
              fileId: req.file.id,
          });

          newImage.save()
              .then((image) => {

                  res.status(200).json({
                      success: true,
                      image,
                  });
              })
              .catch(err => res.status(500).json(err));
      })
      .catch(err => {
        console.log(err, '------------')
        res.status(500).json(err)
      });
})

module.exports = router;