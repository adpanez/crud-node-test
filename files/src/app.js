const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const routes = require('./routes/routes');
const { DB_URL, PORT } = require('./config');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');

mongoose.Promise = require('bluebird');

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => {
    const app = express();

    app.use(cors());
    
    app.use(methodOverride('_method'));
    
    app.use(express.json());
    
    app.use(express.urlencoded({ extended: false }));
    
    app.use(cookieParser());
    
    app.use(routes);
    
    app.listen(PORT, () => {
      console.log('server running in http://localhost:' + PORT)
    })
  });

mongoose.connection.on('error', err => {
  console.error(err);
  console.log('MongoDB connection failed: ' + db);
  process.exit();
});