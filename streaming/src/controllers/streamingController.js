const services = require('../services/streamingService');

exports.streamFile = async (req, res) => {
  try {
    const response = await services.streamFile(req);

    res.send(response)
  }
  catch (err) {
    res.status(500).send(err);
  }
}