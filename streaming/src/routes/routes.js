const express = require('express');
const router = express.Router();

const streamingController = require('../controllers/streamingController');

router.post('/upload-file', streamingController.streamFile);

module.exports = router;