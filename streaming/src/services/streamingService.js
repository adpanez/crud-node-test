const path = require('path');
const utils = require('../utils/handleFiles');
const streams = require('../streams/streamingWriting');

exports.streamFile = async (req) => {
  const file = req.files.document;

  const uploadPath = './src/public/doc.txt';

  await utils.uploadFile(file, uploadPath);

  try {
    const responseEndSteam = await streams.streamWritingCopyFile(req, uploadPath);

    return {message: responseEndSteam, status: 'OK'};
  } catch (err) {
    return {message: err, status: 'Error'};
  }
}