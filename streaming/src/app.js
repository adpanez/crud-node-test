'use strict';
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const path = require('path');
const routes = require('./routes/routes');

const app = express();
const PORT = 8081;

app.use(fileUpload());

app.use(cors());

app.use(routes);

app.listen(PORT, () => {
  console.log('server running in http://localhost:' + PORT)
})