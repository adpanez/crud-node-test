const path = require('path');
const fs = require('fs');


exports.streamWritingCopyFile = (req, filePath) => {
  const filePath2 = path.join(__dirname, '../public/doc3.txt');
  const document = req.files.document;

  return new Promise((resolve, reject) => {
    const readingStreaming = fs.createReadStream(filePath);
    const writtingSecondStreaming = fs.createWriteStream(filePath2);

    readingStreaming.on('data', chunk => {
      const dataChunk = chunk.toString();
      const replacedDataChunk = dataChunk.replace(/Hola, que tal/g, 'Muy bien, gracias');

      writtingSecondStreaming.write(replacedDataChunk);
    });

    writtingSecondStreaming.on('error', err => {
      console.log('Ocurrió un error al modificar el archivo\n', { err });
    });

    readingStreaming.on('error', err => {
      console.log('Ocurrió un error al leer el archivo\n', { err });
      reject(err);
    });

    readingStreaming.on('close', () => {
      writtingSecondStreaming.close();
      console.log('\n Proceso finalizado\n');
      resolve(filePath2)
    });
  });
}