import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import CreateUser from './views/CreateUser/CreateUser';
import Home from './views/Home/Home';
import Users from './views/Users/Users';
import User from './views/User/User';
import UploadFile from './views/UploadFIle/UploadFile';

function App() {
  return (
    <div className="App">
      <React.Suspense fallback="Loading...">
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/usuarios" component={Users} />
            <Route exact path="/crear-usuario" component={CreateUser} />
            <Route exact path="/usuario/:id" component={User} />
            <Route exact path="/subir-archivo" component={UploadFile} />
          </Switch>
        </Router>
      </React.Suspense>
    </div>
  );
}

export default App;
