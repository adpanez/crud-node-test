import axios from 'axios';
import { Link } from 'react-router-dom';
import { URL_API } from '../../config';
import './UserList.css';

const UserList = ({users}) => {
  const handleDelete = (id) => {
    axios(`${URL_API}/user/${id}`, {
        method: 'DELETE',
    })
      .then(res => {
        const { data } = res.data;
        alert(data);
      })
  }

  return (
    <>
      {users.map(user => {
        const {_id, name, email} = user;
        return (
          <div key={_id} className="body-list">
            <div className="id">{_id}</div>
            <div className="name">{name}</div>
            <div className="email">{email}</div>
            <div className="view">
              <Link to={'/usuario/' + _id}>
                <button>Ver</button>
              </Link>
            </div>
            <div className="delete">
              <button onClick={() => handleDelete(_id)}>Eliminar</button>
            </div>
          </div>
        )
      })}
    </>
  )
}

export default UserList;