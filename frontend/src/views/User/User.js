
import { useEffect, useState } from "react";
import axios from 'axios';
import { URL_API } from "../../config";
import { useForm } from "react-hook-form";

const User = () => {
  const { handleSubmit } = useForm();
  const [user, setUser] = useState(null);
  const [logo, setLogo] = useState(null);
  const id = window.location.pathname.replace('/usuario/', '');

  useEffect(() => {

    axios.get(`${URL_API}/user/${id}`)
      .then(res => {
        setUser(res.data.data)
      })
      .catch(() => console.log('Hubo un error'))
  }, [id])

  const handleChange = (image) => {
    setLogo(image);
  }

  const onSubmit = () => {
    const formData = new FormData();
    formData.append('logo', logo);

    axios.post(`${URL_API}/upload-logo/${id}`, formData)
      .then(res => {
        const { message } = res.data;
        alert(message);
      })
      .catch(() => console.log('Hubo un error'))
  }

  return (
    <div>
      <h1>Listado de usuarios</h1>
      <div className="">
        Usuario
      </div>
      {
        user ? (
          <div className="user">
            <div className="id">{user.id}</div>
            <div className="name">{user.name}</div>
            <div className="name">{user.lastName}</div>
            <div className="email">{user.email}</div>
            <div className="birthday">{user.birthday}</div>
            <div className="gender">{user.gender}</div>
            <div className="phone">{user.phone}</div>
            <div className="logo">{user.logo ? 'Tiene logo' : 'Aun no tiene logo'}</div>
            <div className="active">{user.active ? 'Si' : 'No'}</div>
          </div>
        ) : <div>Cargando datos</div>
      }

      <div>
        {user && (
          <div>
            <form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
              <div>
                <label>Logo</label>
                <input type="file" onChange={(event) => handleChange(event.target.files[0])} accept="image/png, image/jpeg"/>
              </div>
              <div><button type="submit">Enviar imagen</button></div>
            </form>
          </div>
        )}
      </div>
    </div>
  )
}

export default User;