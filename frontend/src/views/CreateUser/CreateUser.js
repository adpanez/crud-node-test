import axios from 'axios';
import { useForm } from 'react-hook-form';
import { URL_API } from '../../config';
import './CreateUser.css';

const CreateUser = () => {
  const { register, handleSubmit } = useForm();

  const onSubmit = data => {
    const convertActive = data.active === 'yes' ? true : false;

    axios.post(`${URL_API}/user`, {...data, active: convertActive})
      .then(res => {
        if (res.data.status === 'OK') {
          const { data } = res.data;
          alert(data );
        } else {
          const { message } = res.data;
          alert(message);
        }
      })
      .catch(() => console.log('ocurrio un error'));
  };

  return (
    <div>
      <h1>Crear nuevo usuario</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="control">
          <label htmlFor="name">Nombre: </label>
          <input type="text" id="name" {...register('name')} />
        </div>
        <div className="control">
          <label htmlFor="lastname">Apellidos: </label>
          <input type="text" id="lastname" {...register('lastname')} />
        </div>
        <div className="control">
          <label htmlFor="email">Email: </label>
          <input type="email" id="email" {...register('email')}/>
        </div>
        <div className="control">
          <label htmlFor="birthday">Fecha de nacimiento: </label>
          <input type="date" id="birthday" {...register('birthday')} />
        </div>
        <div className="control">
          <label>Género: </label>
          <input type="radio" value="male" {...register('gender')} /> Hombre
          <input type="radio" value="female" {...register('gender')} /> Mujer
        </div>
        <div className="control">
          <label htmlFor="phone">Número de telefono: </label>
          <input type="number" id="phone" {...register('phone')} />
        </div>
        <div className="control">
          <label>¿Trabaja?: </label>
          <input type="radio" value="yes" {...register('active')} /> Si
          <input type="radio" value="no" {...register('active')} /> No
        </div>
        <div className="control">
          <button>Enviar</button>
        </div>
      </form>
    </div>
  )
}

export default CreateUser;