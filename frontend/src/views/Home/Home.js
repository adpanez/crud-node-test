import { Link } from "react-router-dom";

const Home = () => (
  <div>
    <h1>Ejemplo de CRUD con node, mongodb y react. Gestión de usuarios</h1>
    <div>
      <Link to="/usuarios">Listado de usuarios</Link>
    </div>
  </div>
)

export default Home;