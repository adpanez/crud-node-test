import { useEffect, useState } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import UserList from "../../components/UserList/UserList";
import { URL_API } from "../../config";
import './Users.css';

const Users = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios.get(URL_API + '/users')
      .then(res => {
        setUsers(res.data.data)
      })
      .catch(() => console.log('Ha ocurrido un error'))
      .finally(() => setLoading(false))
  }, [])

  return (
    <div>
      <h1>Listado de usuarios</h1>
      <div className="create">
        <Link to="/crear-usuario">Crear nuevo usuario</Link>
      </div>
      <div className="header-list">
        <div className="id">ID</div>
        <div className="name">Nombre</div>
        <div className="email">Email</div>
        <div className="view"></div>
        <div className="delete"></div>
      </div>
      {loading ? <div>Cargando...</div> : <UserList users={users}/>}
    </div>
  )
}

export default Users;