
import { useState } from "react";
import axios from 'axios';
import { URL_API_STREAMING } from "../../config";
import { useForm } from "react-hook-form";

const UploadFile = () => {
  const [document, setDocument] = useState(null);
  const { handleSubmit } = useForm();

  const onSubmit = () => {
    const formData = new FormData();
    formData.append('document', document);

    axios.post(`${URL_API_STREAMING}/rewrite-document`, formData)
      .then(res => {
        console.log('Subido')
      })
      .catch(() => console.log('Hubo un error'))
  }

  const handleChange = (file) => {
    setDocument(file)
  }

  return (
    <div>
      <h1>Sube un documento</h1>
      <div>
        <form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label>Logo</label>
            <input type="file" onChange={(event) => handleChange(event.target.files[0])} accept="txt"/>
          </div>
          <div><button type="submit">Enviar document</button></div>
        </form>
      </div>
    </div>
  )
}

export default UploadFile;